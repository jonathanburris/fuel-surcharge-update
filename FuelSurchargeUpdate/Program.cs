﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FuelSurchargeUpdate
{
    class Program
    {
        class Options
        {
            [Option('u', "url", Required = false, DefaultValue = "http://www.eia.gov/petroleum/gasdiesel/xls/",
              HelpText = "Input url to be processed.")]
            public string Url { get; set; }

            [Option('f', "file", Required = false, DefaultValue = "psw18vwall.xls",
              HelpText = "Input file to be processed.")]
            public string FileName { get; set; }

            [Option('o', "output", Required = false, DefaultValue = "output.txt",
              HelpText = "Output file to be updated.")]
            public string OutputFileName { get; set; }

            [Option('v', "verbose", Required = false, DefaultValue = false,
              HelpText = "Verbose logging.")]
            public bool Verbose { get; set; }

            [Option('h', "help", Required = false, DefaultValue = false,
              HelpText = "Help.")]
            public bool Help { get; set; }

            [ParserState]
            public IParserState LastParserState { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                return HelpText.AutoBuild(this,
                  (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
            }
        }

        static string url;
        static string fileName;
        static string outputFileName;

        static string assemblyLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
        static string assemblyFolder = System.IO.Path.GetDirectoryName(assemblyLocation);

        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                //CommandLine values are available
                url = options.Url;
                fileName = options.FileName;
                outputFileName = options.OutputFileName;

                if (options.Help)
                {
                    Console.WriteLine(options.GetUsage());
                    return;
                }
                
            }

            downloadFile(url, fileName);

            extractData();

            if (options.Verbose)
            {
                openFolder();
                openFile();
                openOutputFile();

                Console.ReadLine();
            }
        }

        static void downloadFile(string url, string fileName)
        {
            Console.WriteLine("Downloading '" + fileName + "' from '" + url + "'");

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(string.Concat(url, fileName), fileName);
                }

                Console.WriteLine("File Download Complete");
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Concat("Error: ", ex.Message));
            }
        }

        static void openFolder() {

            Process.Start(assemblyFolder);

        }

        static void openFile()
        {
            Console.WriteLine(string.Format("Opening: {0}", fileName));

            Process.Start(fileName);
        }

        static void openOutputFile()
        {
            Console.WriteLine(string.Format("Opening: {0}", outputFileName));

            Process.Start(outputFileName);
        }

        static void extractData()
        {
            var workbook = ExcelLibrary.SpreadSheet.Workbook.Load(fileName);
            var worksheet = workbook.Worksheets[5];

            DateTime lastUpdateDate = (DateTime)worksheet.Cells[worksheet.Cells.LastRowIndex, 0].DateTimeValue;
            var lastPrice = worksheet.Cells[worksheet.Cells.LastRowIndex, 1].Value;

            using (TextWriter writer = File.CreateText(outputFileName))
            {
                writer.WriteLine(string.Format("{0}|{1}", lastUpdateDate.ToString("MM/dd/yyyy"), lastPrice));
            }

            Console.WriteLine(string.Format("{0} has been updated", outputFileName));
        }
    }
}
